# Refactoring by Examples

This code repo shows refactoring examples for known code smells.

Both code smells and their respective refactorings exposed here follow guides and principles of the second edition of the
book `Refactoring` by Martin Fowler.

This repo contains two branches.

* The **before** branch has the code with all smells we want to get rid of.
* The **after** branch has the code with refactorings applied
* Every function/object that we want to refactor has its respective test.

## Important takes

> Refactoring is about changing the internal structure of software to make it easier to understand and cheaper
> to change **without changing its observable behaviour**.
> 
> Before you start refactoring, make sure you have a solid suite of **tests**
> 
> -- <cite>Martin Fowler</cite>

![alt Refactoring](refactoring.svg)

## Code Smells

### Mysterious Name
* Machines do not care about naming, **but humans do**.
### Duplicated Code
* Having the same expression in the code of a class/module
### Long Function
* The longer a function is, the more difficult is it to understand
* Result of several levels of abstractions mixed within a single scope
### Long Parameter List [T.B.D]
### Global Data [T.B.D]
### Mutable Data [T.B.D]
### Divergent Change [T.B.D]
### Shotgun Surgery [T.B.D]
### Feature Envy [T.B.D]
### Data Clumps [T.B.D]
### Primitive Obsession [T.B.D]
### Repeated Switches [T.B.D]
### Loops [T.B.D]
### Speculative Generality [T.B.D]
### Temporary Field [T.B.D]
### Message Chains [T.B.D]
### Middle Man [T.B.D]
### Insider Trading [T.B.D]
### Large Class [T.B.D] 
### Alternative Classes with Different Interfaces [T.B.D]
### Data Class [T.B.D]
### Refused Bequest [T.B.D]
### Comments
* Comments should usually tell the **why**, not the **what**. 
* Even a single line is worth extracting if it needs explanation

## References

1. Refactoring by Martin Fowler. [click here](https://martinfowler.com/tags/refactoring.html)
2. Code Refactoring: Learn Code Smells And Level Up Your Game!. [click here](https://www.youtube.com/watch?v=D4auWwMsEnY)
3. Smells to Refactorings - Quick Reference Guide. [click here](https://www.industriallogic.com/img/blog/2005/09/smellstorefactorings.pdf)