from refactoring_by_examples.mysterious_name import a


class TestMysteriousName:

    def test_should_succeed(self):
        assert a(5, 10) == 2500
