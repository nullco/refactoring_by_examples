from refactoring_by_examples.long_function import magic_number


class TestLongFunction:

    def test_should_succeed(self):
        assert magic_number(50) == 4547
