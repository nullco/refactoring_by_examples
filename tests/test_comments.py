from refactoring_by_examples.comments import magic_number


class TestComments:

    def test_should_succeed(self):
        assert magic_number(100) == 17844
