from refactoring_by_examples.duplicated_code import score_words, score_even_words


class TestDuplicatedCode:

    def test_should_score_words(self):
        words = ['hello', 'world', 'from', 'python']
        assert score_words(words) == 18

    def test_should_score_even_words_only(self):
        words = ['hello', 'world', 'from', 'python']
        assert score_even_words(words) == 8
