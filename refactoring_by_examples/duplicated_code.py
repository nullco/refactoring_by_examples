
def score_words(words):
    score = 0
    for word in words:
        for character in word:
            if character == 'a':
                score += 1
            elif character == 'e':
                score += 2
            elif character == 'i':
                score += 3
            elif character == 'o':
                score += 4
            elif character == 'u':
                score += 5
    return score


def score_even_words(words):
    score = 0
    for index, word in enumerate(words):
        if index % 2 == 0:
            continue
        for character in word:
            if character == 'a':
                score += 1
            elif character == 'e':
                score += 2
            elif character == 'i':
                score += 3
            elif character == 'o':
                score += 4
            elif character == 'u':
                score += 5
    return score


