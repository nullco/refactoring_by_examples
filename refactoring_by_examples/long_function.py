
def magic_number(number):

    result = number
    result = result * number

    for n in range(0, number + 1):
        result += n

    for n in range(0, number + 1, 2):
        result += n

    accumulated_digits = ""
    for n in range(0, number + 1):
        accumulated_digits += str(n)

    result += len(accumulated_digits)

    all_numbers = []
    for n in range(0, number + 1):
        all_numbers.append(n)

    result += sum(all_numbers) // len(all_numbers)

    reversed_digits = str(number)[::-1]
    result += int(reversed_digits)

    return result




